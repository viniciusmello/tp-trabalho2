package pacote;

public class Mac {
	private String mac = new String();
	
	public Mac(String mac){
		this.mac = mac;
	}
	
	public String getIpBynaryString(){
		StringBuilder mac_builder = new StringBuilder();
		String[] componentes_mac = mac.split("\\.");
		
		if(componentes_mac.length != 6)
			System.out.println("Formato invalido de MAC");

		componentes_mac[0] = Integer.toBinaryString(Integer.valueOf(componentes_mac[0]));
		componentes_mac[1] = Integer.toBinaryString(Integer.valueOf(componentes_mac[1]));
		componentes_mac[2] = Integer.toBinaryString(Integer.valueOf(componentes_mac[2]));
		componentes_mac[3] = Integer.toBinaryString(Integer.valueOf(componentes_mac[3]));
		componentes_mac[4] = Integer.toBinaryString(Integer.valueOf(componentes_mac[4]));
		componentes_mac[5] = Integer.toBinaryString(Integer.valueOf(componentes_mac[5]));
		
		for(int i = componentes_mac[0].length() ; i < 8 ; i++) componentes_mac[0] = '0'+ componentes_mac[0];
		for(int i = componentes_mac[1].length() ; i < 8 ; i++) componentes_mac[1] = '0'+ componentes_mac[1];
		for(int i = componentes_mac[2].length() ; i < 8 ; i++) componentes_mac[2] = '0'+ componentes_mac[2];
		for(int i = componentes_mac[3].length() ; i < 8 ; i++) componentes_mac[3] = '0'+ componentes_mac[3];
		for(int i = componentes_mac[4].length() ; i < 8 ; i++) componentes_mac[4] = '0'+ componentes_mac[4];
		for(int i = componentes_mac[5].length() ; i < 8 ; i++) componentes_mac[5] = '0'+ componentes_mac[5];
		
		mac_builder.append(componentes_mac[0]);
		mac_builder.append(componentes_mac[1]);
		mac_builder.append(componentes_mac[2]);
		mac_builder.append(componentes_mac[3]);
		mac_builder.append(componentes_mac[4]);
		mac_builder.append(componentes_mac[5]);
		
		return mac_builder.toString();
	}

}
