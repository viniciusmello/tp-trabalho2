package pacote;

public class RoteadorDeBorda {
	private Ip ip_host; 
	private Ip ip_roteador_borda;
	private Mac end_mac;
	private Porta porta;
	
	public boolean N_Activate_Request(){
		//TODO se identificar como roteador de borda(ip_host = ip_roteador_borda). Fazer conex�o logica(emular liga��o ponto a ponto) com hoteador central, informando informando mascara de rede e IP base
		
		if(!inicializaNiveisDeRede()) return false;
		if(!inicializaNiveisDeEnlace()) return false;
		if(!inicializaNiveisFisicos()) return false;
		
		return true;
	}
	
	//Solicita transmiss�o de um datagrama
	public void N_Data_Request(Protocolo protocolo, RoteadorDeBorda host, Ip ip_destino, Datagrama datagrama_a_enviar, int numero_bytes){
		Mac end_mac_destino = getMac(ip_destino);
		//TODO enviar datagrama
	}

	//Testa se h� um datagrama recebido no n�vel de rede
	public boolean N_Data_Indication(Protocolo protocolo){
		//TODO checar se datagrama foi recebido na camada de rede. Retornar true caso sim e false caso contr�rio
		return false;
	}
	
	//Busca no n�vel de rede os dados do �ltimo datagrama recebido
	public int N_Data_Receive(Protocolo protocolo, Ip ip_sender, Datagrama datagrama_recebido, int datagrama_data_max_size){
		//TODO checar se datagrama � destinado a outra subrede. Caso seja, encaminhas o datagrama para roteador central
		//TODO retornar numero de bytes do campo de dados ou -1 em caso de falha
		return -1;
	}
	
	//Finaliza o funcionamento do n�veis de rede
	public void N_Deactivate_Request(){
		//TODO finalizar funcionamento dos n�veis de rede
	}
	
	private boolean inicializaNiveisDeRede(){
		//TODO receber endere�o IP do roteador de borda (IP do roteador de borda == IP do host)
		//TODO receber endere�o IP do host (IP do roteador de borda == IP do host)
		//TODO conectar-se logicamente com roteador central
		return false;
	}
	
	private boolean inicializaNiveisDeEnlace(){
		//TODO receber endere�o MAC da maquina
		return false;
	}

	private boolean inicializaNiveisFisicos(){
		//TODO receber porta usada para comunica��o default (5000)
		//TODO receber IP do comutador de enlace
		return false;
	}
	
	private Mac getMac(Ip ip_destino) {
		// TODO Usar ARP ou tabela de mapeamento fixa de IP-MAC
		return null;
	}
}
