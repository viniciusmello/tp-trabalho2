package pacote;

public class Ip {
	private String ip = new String();
	
	public Ip(String ip){
		this.ip = ip;
	}
	
	public String getIpBynaryString(){
		StringBuilder ip_builder = new StringBuilder();
		String[] componentes_ip = ip.split("\\.");
		
		if(componentes_ip.length != 4)
			System.out.println("Formato invalido de IP");

		componentes_ip[0] = Integer.toBinaryString(Integer.valueOf(componentes_ip[0]));
		componentes_ip[1] = Integer.toBinaryString(Integer.valueOf(componentes_ip[1]));
		componentes_ip[2] = Integer.toBinaryString(Integer.valueOf(componentes_ip[2]));
		componentes_ip[3] = Integer.toBinaryString(Integer.valueOf(componentes_ip[3]));
		
		for(int i = componentes_ip[0].length() ; i < 8 ; i++) componentes_ip[0] = '0'+ componentes_ip[0];
		for(int i = componentes_ip[1].length() ; i < 8 ; i++) componentes_ip[1] = '0'+ componentes_ip[1];
		for(int i = componentes_ip[2].length() ; i < 8 ; i++) componentes_ip[2] = '0'+ componentes_ip[2];
		for(int i = componentes_ip[3].length() ; i < 8 ; i++) componentes_ip[3] = '0'+ componentes_ip[3];
		
		ip_builder.append(componentes_ip[0]);
		ip_builder.append(componentes_ip[1]);
		ip_builder.append(componentes_ip[2]);
		ip_builder.append(componentes_ip[3]);
		
		return ip_builder.toString();
	}

}
