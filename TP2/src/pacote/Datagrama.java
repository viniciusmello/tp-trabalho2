package pacote;


public class Datagrama {
	private Integer versao = 6;
	private Integer classe_de_trafego = 0;
	private Integer identificador_de_fluxo = 0;
	private Integer tamanho_dos_dados = 0;
	private Integer proximo_cabecalho = 0;
	private Integer limite_de_encaminhamento = 0;
	private Integer dados = 0;
	private Ip ip_origem;
	private Ip ip_destino;

	private int tamanho_versao = 4;
	private int tamanho_classe_de_trafego = 8;
	private int tamanho_identificador_de_fluxo = 20;
	private int tamanho_tamanho_dos_dados = 16;
	private int tamanho_proximo_cabecalho = 8;
	private int tamanho_limite_de_encaminhamento = 8;
	private int tamanho_minimo_dados = 0;

	@SuppressWarnings("unused")
	private Datagrama(){}
	
	public Datagrama(Ip end_origem, Ip end_destino, Integer tamanho_dos_dados,  Integer dados){
		this.tamanho_dos_dados = tamanho_dos_dados;
		this.ip_origem = end_origem;
		this.ip_destino = end_destino;
		this.dados = dados;
	}
	
	public String getDatagramaString(){
		String string_versao = Integer.toBinaryString(versao);
		String string_classe_de_trafego = Integer.toBinaryString(classe_de_trafego);
		String string_identificador_de_fluxo = Integer.toBinaryString(identificador_de_fluxo);
		String string_tamanho_dos_dados = Integer.toBinaryString(tamanho_dos_dados);
		String string_proximo_cabecalho = Integer.toBinaryString(proximo_cabecalho);
		String string_limite_de_encaminhamento = Integer.toBinaryString(limite_de_encaminhamento);
		String string_ip_origem = ip_origem.getIpBynaryString();
		String string_ip_destino = ip_destino.getIpBynaryString();
		String string_dados = Integer.toBinaryString(dados);
		
		for(int i = string_versao.length() ; i < tamanho_versao ; i++) string_versao = '0'+ string_versao;
		for(int i = string_classe_de_trafego.length() ; i < tamanho_classe_de_trafego ; i++) string_classe_de_trafego = '0'+ string_classe_de_trafego;
		for(int i = string_identificador_de_fluxo.length() ; i < tamanho_identificador_de_fluxo ; i++) string_identificador_de_fluxo = '0'+ string_identificador_de_fluxo;
		for(int i = string_tamanho_dos_dados.length() ; i < tamanho_tamanho_dos_dados ; i++) string_tamanho_dos_dados = '0'+ string_tamanho_dos_dados;
		for(int i = string_proximo_cabecalho.length() ; i < tamanho_proximo_cabecalho ; i++) string_proximo_cabecalho = '0'+ string_proximo_cabecalho;
		for(int i = string_limite_de_encaminhamento.length() ; i < tamanho_limite_de_encaminhamento ; i++) string_limite_de_encaminhamento = '0'+ string_limite_de_encaminhamento;
		for(int i = string_dados.length() ; i < tamanho_minimo_dados ; i++) string_dados = '0'+ string_dados;
			
		
		StringBuilder datagrama = new StringBuilder();
		
		datagrama.append(string_versao);
		datagrama.append(string_classe_de_trafego);
		datagrama.append(string_identificador_de_fluxo);
		datagrama.append(string_tamanho_dos_dados);
		datagrama.append(string_proximo_cabecalho);
		datagrama.append(string_limite_de_encaminhamento);
		datagrama.append(string_ip_origem);
		datagrama.append(string_ip_destino);
		datagrama.append(string_dados);

		return datagrama.toString();
	}
}
